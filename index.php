<?php

use App\TemperatureConvertor;
use Symfony\Component\Console\Application;


require __DIR__ . '/vendor/autoload.php';

$application = new Application();

// ... register commands
$application->add(new TemperatureConvertor);

$application->run();
