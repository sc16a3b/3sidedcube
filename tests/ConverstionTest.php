<?php

namespace Tests;

use App\TemperatureConvertor;
use PHPUnit\Framework\TestCase;

class ConverstionTest extends TestCase
{
   /**
    * @dataProvider conversions
    */
   public function testConversion(int $degree, int $expectedResult, bool $fahrenheit = false)
   {
      $this->assertSame($expectedResult, TemperatureConvertor::convert($degree, $fahrenheit));
   }

   public function conversions(): array
   {
      return [
         [-20, -4],
         [30, 86],
         [0, 32],
         [10, 50],
         [40, 104],
         [100, 212],
         [-40, -40, true],
         [-10, -23, true],
         [0, -18, true],
         [32, 0, true],
         [70, 21, true],
         [86, 30, true],
      ];
   }
}
