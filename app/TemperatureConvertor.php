<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TemperatureConvertor extends Command
{
   // the name of the command (the part after "bin/console")
   protected static $defaultName = 'temperature:convert';
   protected static $defaultDescription = 'Converts a temperature from Celcius to Fahrenheit or vice versa.';

   protected function configure(): void
   {
      $this->addArgument('degree', InputArgument::REQUIRED, 'Degree to be converted')
         ->addOption(
            'fahrenheit',
            'f',
            InputOption::VALUE_NONE,
            'Degree to be converted is in Fahrenheit',
            null
         );
   }

   protected function execute(InputInterface $input, OutputInterface $output): int
   {
      if ($input->getOption('fahrenheit')) {
         $output->writeln("Temperature in Celsius: " . self::convert($input->getArgument('degree'), 1));
      } else {
         $output->writeln("Temperature in Fahrenheit: " . self::convert($input->getArgument('degree')));
      }

      return Command::SUCCESS;
      // return Command::FAILURE;
      // return Command::INVALID
   }

   public static function convert(int $degree, bool $fahrenheit = false): int
   {
      if ($fahrenheit) {
         return number_format(($degree - 32) * 0.5556, 0);
      }
      return number_format(($degree * 1.8) + 32, 0);
   }
}
